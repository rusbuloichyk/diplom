var menu = document.querySelector('.menu');
var clickLink = document.querySelectorAll('.menu-nav__link:not(:first-child)');
var close = document.querySelector('.close-menu');
var go = document.querySelector('.start-menu');

for (var i = 0; i < clickLink.length; i++) {
    clickLink[i].onclick = function () {
        menu.classList.add('active');
        close.click();
    };
};

go.onclick = function() {
    menu.classList.remove('active');
}