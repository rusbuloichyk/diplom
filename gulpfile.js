const gulp = require("gulp");
const concat = require("gulp-concat");
const browserSync = require("browser-sync");
const sass = require("gulp-sass");
const del = require("del");
const autoprefixer = require("gulp-autoprefixer");
const pug = require("gulp-pug");

// Таски

// gulp.task('html', function () {
//     gulp.src('./src/**/*.html')
//         .pipe(gulp.dest('./dist'));
// });
gulp.task('js', function () {
    gulp.src('./src/**/*.js')
        .pipe(gulp.dest('./dist'));
});

gulp.task('fonts', function () {
    gulp.src('./src/**/fonts/**')
        .pipe(gulp.dest('./dist'));
});

gulp.task('images', function () {
    gulp.src('./src/**/images/**/*')
        .pipe(gulp.dest('./dist'));
});

gulp.task('pug', function () {
    return gulp.src('./src/**/*.pug')
        .pipe(pug( {
            pretty:true
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('sass', function () {
    return gulp.src('./src/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 15 versions'],
        }))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./dist/styles'));
});

gulp.task('reloader', function () {
    browserSync({
        server: {
            baseDir: './dist'
        }
    });
});

gulp.task('del', function () {
    return del.sync('dist');
});

gulp.task("watch", ['del', 'sass', 'fonts', 'images', 'pug', 'reloader', 'js'], function () {
    gulp.watch('./src/**/*.pug', ['pug']);
    gulp.watch('./src/**/*.js', ['js']);
    gulp.watch('./dist/**/*.js', browserSync.reload);
    gulp.watch('./src/**/fonts/**', ['fonts']);
    gulp.watch('./src/**/images/**/*', ['images']);
    gulp.watch('./dist/**/*.html', browserSync.reload);
    gulp.watch('./src/sass/*.scss', ['sass']);
    gulp.watch('./dist/styles/**/*.css').on('change', browserSync.reload);
});